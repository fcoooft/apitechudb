package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.PurchaseModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PurchaseRepository {

    public List<PurchaseModel> findAll(){
        System.out.println("findAll en PurchaseRepository");

        return ApitechudbApplication.purchaseModels;
    }

    public PurchaseModel save(PurchaseModel purchase){
        System.out.println("Save en UserRepository");

        ApitechudbApplication.purchaseModels.add(purchase);

        return purchase;
    }

    public Optional<PurchaseModel> findById(String id){
        System.out.println("findById en PurchaseRepository");

        Optional<PurchaseModel> result = Optional.empty();

        for (PurchaseModel purchaseInList : ApitechudbApplication.purchaseModels) {
            if(purchaseInList.getId().equals(id)) {
                System.out.println("Usuario encontrado con la id" + id);
                result = Optional.of(purchaseInList);
            }
        }
        return result;
    }
}
