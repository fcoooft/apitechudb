package com.techu.apitechudb.controllers;


import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v3")
public class UserController {

    @Autowired
    UserService userService;

    //Mostrar todos los usuarios por una edad determinada
    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(name = "ageFilter", defaultValue = "0") int ageFilter){
        System.out.println("getUsers");
        System.out.println("El valor de ageFilter es " + ageFilter);

        return new ResponseEntity<>(
                this.userService.findAll(ageFilter),
                HttpStatus.OK
        );
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("la id del usuario a buscar es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }


    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");
        System.out.println("la id del usuario a crear es " + user.getId());
        System.out.println("El nombre del usuario a crear es " + user.getName());
        System.out.println("La edad del usuario a crear es " + user.getAge());

        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED
        );
    }


    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id) {
        System.out.println("deleteUser");
        System.out.println("La id del usuario a borrar es " + id);

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Usuario borrado" : "Usuario no encontrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }


    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id){
        System.out.println("La id del usuario que se va a actualizar en URL es " + id);
        System.out.println("La id del usuario que se va a actualizar es " + user.getId());
        System.out.println("El nombre del usuario que se va a actualizar es " + user.getName());
        System.out.println("La edad del usuario que se va a actualizar es " + user.getAge());

        return new ResponseEntity<>(this.userService.update(user), HttpStatus.OK);
    }
}
